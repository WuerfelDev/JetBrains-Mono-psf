#!/bin/bash


# Clean old files
rm font.bdf
rm font.psf


# Download ttf file
[[ ! -f "font.ttf" ]] && curl -o font.ttf https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/fonts/ttf/JetBrainsMonoNL-Regular.ttf


# r: resolution
# p: point size
# ttf to bdf
otf2bdf -r 70 -p 16 -o font.bdf  font.ttf
grep 'AVERAGE_WIDTH' font.bdf
sed -i -e "s/AVERAGE_WIDTH.*/AVERAGE_WIDTH 100/" font.bdf


# bdf to psf
bdf2psf --fb font.bdf /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/Uni2.512 512 font.psf 2> /dev/null
echo "return code $?"
