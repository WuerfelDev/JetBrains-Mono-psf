#!/bin/bash

version='1.0'

show_help () {
    # Print help
    echo "Create image of a command run in tty terminal using a provided font."
    echo "Usage: $(basename "$0") (OPTIONS) ..font"
    echo "       $(basename "$0") (OPTIONS) ..directory"
    echo -e "\nOptions:"
    echo " -c --command COMMNAND  Execute a custom commnand"
    echo "    --crop              cropped image"
    echo " -h --help              Show this help"
    echo " -v --version           Shows the program version"
}


stop_reset () {
    echo -e '\033[?25h'
    setfont
    trap - INT
    exit "$1"
}

[[ $# -eq 0 ]] && show_help && exit 0


trap 'echo "Received SIGINT. Exiting";stop_reset 1' INT


# Handle parameters
cmnd='showconsolefont'
ttyshot='fbgrab'
fonts=()
next=0
for ARG in "$@";
do
    if [[ $ARG == "--help" || $ARG == "-h" ]]
    then
        # Print help
        show_help
        exit 0
    elif [[ $ARG == "--version" || $ARG == "-v" ]]
    then
        # Print version
        echo "$(basename "$0") version $version"
        exit 0
    elif [[ $next -ne 0 ]]
    then
        # Set commnand
        cmnd=$ARG
        next=0
    elif [[ $ARG == "--command" || $ARG == "-c" ]]
    then
        # Mark next argument as command
        next=1
    elif [[ $ARG == "--crop" ]]
    then
        ttyshot="not-fbgrab"
    else
        # Add font to array
        if [[ -f "$ARG" ]]
        then
            fonts+=( "$ARG" )
        elif [[ -d "$ARG" ]]
        then
            fonts+=( $(find "$ARG" -maxdepth 1 -type f -iname '*.psfu.gz') )
            fonts+=( $(find "$ARG" -maxdepth 1 -type f -iname '*.psf.gz') )
            fonts+=( $(find "$ARG" -maxdepth 1 -type f -iname '*.psf') )
            fonts+=( $(find "$ARG" -maxdepth 1 -type f -iname '*.psfu') ) # This matches README.psfu. Filtered in main loop
        fi
    fi
done


# Exit if not tty.
# XDG_SESSION_TYPE not set when started with sudo
# $TERM/$(tty) is *pts* for tmux
[[ -n "$DISPLAY" ]] && echo "Run this script from a tty shell." && exit 1

# Run with sudo, alternativly run "sudo -n true ||"
[[ $EUID -ne 0 ]] && echo "This script must be run as root." && exec sudo "$0" "$@"



# Prepare: reset terminal, hide cursor
reset
echo -e '\033[?25l'


# Loop through fonts array
for font in "${fonts[@]}";
do
    fontname=$(basename "${font%.*}")
    [[ $fontname == "README" ]] && continue

    setfont "$font"
    clear
    echo "Using font $fontname"
    sudo -u "$(logname)" bash -c "$cmnd"
    if [[ $ttyshot == "fbgrab" ]]
    then
        fbgrab "$fontname.png" 2> /dev/null
    else
        # This is nicely cropped
        fbdump | pnmcrop -black -margin=8 | pnm2png > "$fontname.png"
    fi
    chown "$(logname)" "$fontname.png"
done


# restore cursor and font
echo "Done."
stop_reset 0
