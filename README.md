# JetBrains Mono PSF

## Direct Download

You can download the latest build here: [font.psf](https://gitlab.com/WuerfelDev/JetBrains-Mono-psf/-/jobs/artifacts/master/raw/font.psf?job=psf).

To test it run `setfont path/to/font.psf` in your tty terminal.
<!--
The files are built in the psf directory.
Use this command to install them directly into your consolefonts directory.
```
curl gitrepo/JetBrains-Mono-psf.tar.gz | tar xzf --directory=/usr/share/kbd/consolefonts/ -
```
-->



## Creating

### Generate Font

`./generate.sh`

[TODO: optimize]

_Turns out downscaled fonts aren't too great_

> Required packages:\
> otf2bdf + bdf2psf

### Test Output

Testing a font works just by generating images of how it looks, then we choose what looks best. For different symbols we can set a custom command.

```
$ ./test-font.sh -h
Create image of a command run in tty terminal using a provided font.
Usage: test-font.sh (OPTIONS) ..font
       test-font.sh (OPTIONS) ..directory

Options:
 -c --command COMMNAND  Execute a custom commnand
    --crop              cropped image
 -h --help              Show this help
 -v --version           Shows the program version
```

To generate images of all installed tty fonts:
```
./test-font.sh /usr/share/kbd/consolefonts/
```

Advantage of fbgrab: Compare font size\
Advantage of cropped: Compare glyph postition

> Required packages:\
> fbgrab (fullscreen image,default) or\
> fbdump + netpbm + libpng (cropped image)


## License

This psf version of JetBrains Mono is a modified derivitive and therefore under the same license: [OFL-1.1 License](OFL.txt)

## Resources

**Font generation**

- https://www.reddit.com/r/linuxquestions/comments/7st7hz/any_way_to_convert_ttf_files_to_psf_files/
- https://unix.stackexchange.com/questions/161890/how-can-i-make-a-psf-font-for-the-console-from-a-otf-one
- https://git.launchpad.net/ubuntu/+source/fonts-ubuntu/tree/debian/console/Makefile
- https://github.com/JetBrains/JetBrainsMono/
- ...

**TTY testing**

- https://web.archive.org/web/20210521162814/https://alexandre.deverteuil.net/docs/archlinux-consolefonts/
- https://github.com/torvalds/linux/blob/master/Documentation/fb/fbcon.rst
- https://github.com/torvalds/linux/blob/master/Documentation/driver-api/console.rst
- https://wiki.archlinux.org/title/Linux_console#Fonts
- ...
